//
//  DetalhesViewController.swift
//  Contato
//
//  Created by Eder Andrade on 31/08/22.
//

import UIKit

class DetalhesViewController: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    @IBAction func excluirContatoTap(_ sender: Any) {
    }
    public var contato: Contato?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
    }
}
